# Solving "言語処理100本ノック 2015" in Haskell

[言語処理100本ノック](http://www.cl.ecei.tohoku.ac.jp/nlp100/)
にて掲載されている問題を Haskell で解いています。

`nlpX.hs` には 第X章 の解答プログラムがあります。

Haskell には不慣れなので、実行効率上問題の有りそうなコードや、もっとエレガントでスマートな書き方、バグやアンチパターンなどにお気付きの方は [Issue](https://bitbucket.org/Iruyan_Zak/nlp100/issues/new) もしくは [Twitter](https://twitter.com/Iruyan_Zak) までご報告ください。
